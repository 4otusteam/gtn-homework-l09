using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GtN.View
{

    //ядро меню
    public static class Menu
    {
        private static readonly int row = Console.CursorTop;
        private static readonly int col = Console.CursorLeft;

        public static int MenuLogic(string[] items, int index)
        {
            DrawMenu drawMenu = new DrawMenu();

            while (true)
            {
                drawMenu.Draw(items, index, col, row);
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.DownArrow:
                        if (index < items.Length - 1)
                            index++;
                        drawMenu.Draw(items, index, col, row);
                        break;
                    case ConsoleKey.UpArrow:
                        if (index > 0)
                            index--;
                        drawMenu.Draw(items, index, col, row);
                        break;
                    case ConsoleKey.Enter:
                        drawMenu.Draw(items, index, col, row);
                        return index;
                }                
            }
        }
    }
}
