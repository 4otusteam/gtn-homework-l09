using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GtN.View.Interfaces
{
    internal interface IDrawMenu
    {
        public void Draw(string[] items, int index, int col, int row);
    }
}
