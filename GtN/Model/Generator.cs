using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GtN.Model
{
    public class Generator: IGenerator
    {
        public int Value { get; set; }

        public Generator(int min, int max)
        {
            Random rnd = new();
            Value = rnd.Next(min, max);
        }
    }
}
