using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GtN.Model;

namespace GtN.Model
{
    public class Play : IPlay
    {
        public void StartGame(Configure configure)
        {
            Console.CursorVisible = true;
            Console.Clear();
            Console.WriteLine("Игра началась!");
            Console.WriteLine($"Отгадайте число от: {configure.min}   до: {configure.max}");
            Generator valueToGuess = new Generator(configure.min, configure.max);
            for (int i = 0; i < configure.tries; i++)
            {
                Console.WriteLine($"Введите число. Попытка: {i + 1} из: {configure.tries}");
                if (int.TryParse(Console.ReadLine(), out int iGuess))
                {
                    if (Check(iGuess, valueToGuess.Value))
                    {
                        Console.WriteLine("Поздравляю, вы угадали!");
                        Console.WriteLine("Нажмите любую клавишу для продолжения");
                        Console.ReadKey();
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("Вы ввели что-то непонятное для меня и количество попыток не уменьшится уменьшится");
                    i--;
                }

                if (i == configure.tries - 1)
                {
                    Console.WriteLine($"Не угадали, повезет в следущий раз. Число было: {valueToGuess.Value}");
                    Console.WriteLine("Нажмите любую клавишу для продолжения");
                    Console.ReadKey();
                }
            }
        }


        private bool Check(int value, int valueToGuess)
        {
            //не горжусь этими кучами if...

            if (value == valueToGuess) return true;


            if (value - valueToGuess > 0)
            {
                Console.WriteLine("ваше число больше того, что нужно угадать");
            }
            else
            {
                Console.WriteLine("ваше число меньше того, что нужно угадать");
            }

            if (Math.Abs(value - valueToGuess) <= 10)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("горячо");
                Console.ResetColor();
            }
            else if (Math.Abs(value - valueToGuess) <= 30)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("тепло");
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("холодно");
                Console.ResetColor();
            }

            return false;
        }

    }
}
