using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GtN.Model
{
    internal interface IConfigure
    {
        int tries { get; set; }
        int min { get; set; }
        int max { get; set; }

        public void Tries();
        public void LowerRange();
        public void UpperRange();
    }
}
