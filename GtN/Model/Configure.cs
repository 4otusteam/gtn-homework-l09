using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GtN.Model
{
    public class Configure : IConfigure
    {
        public int tries { get; set; }
        public int min { get; set; }
        public int max { get; set; }
     
        public Configure(int tries, int min, int max)
        {
            this.tries = tries;
            this.min = min;
            this.max = max;
        }

        public void Tries()
        {
            Console.CursorVisible = true;
            Console.Clear();
            Console.WriteLine($"Задайте количество попыток. Текущее значение: {tries}");
            if (int.TryParse(Console.ReadLine(), out int iTries))
            {
                tries = iTries;
            }
            else
            {
                Console.WriteLine("Число слишком большое, или вообще не число");
            }
            Console.WriteLine("Нажмите любую клавишу для продолжения");
            Console.ReadKey();            
        }
        public void LowerRange()
        {
            Console.CursorVisible = true;
            Console.Clear();
            Console.WriteLine("Задайте нижнюю границу диапазона для угадывания (не менее 100 от верхней).");
            Console.WriteLine($"Текущее значение нижней границы: {min}   верхней границы: {max}");
            if ((int.TryParse(Console.ReadLine(), out int iMin)) & (max - iMin > 100))
            {
                min = iMin;
                Console.WriteLine($"Текущее значение нижней границы: {min}   верхней границы: {max}");
            }
            else
            {
                Console.WriteLine("Число слишком большое, вообще не число или некорректный диапазон");
            }
            Console.WriteLine("Нажмите любую клавишу для продолжения");
            Console.ReadKey();            
        }

        public void UpperRange()
        {
            Console.CursorVisible = true;
            Console.Clear();
            Console.WriteLine("Задайте верхнюю границу диапазона для угадывания (не менее 100 от верхней).");
            Console.WriteLine($"Текущее значение нижней границы: {min}   верхней границы: {max}");
            if ((int.TryParse(Console.ReadLine(), out int iMax)) & (iMax - min > 100))
            {
                max = iMax;
                Console.WriteLine($"Текущее значение нижней границы: {min}   верхней границы: {max}");
            }
            else
            {
                Console.WriteLine("Число слишком большое, вообще не число или некорректный диапазон");
            }
            Console.WriteLine("Нажмите любую клавишу для продолжения");
            Console.ReadKey();            
        }


    }
}
