using GtN.View;
using GtN.Model;

public class Program
{
    static void Main(string[] args)
    {
        Configure configure = new Configure(10, 0, 1000);        
        int index = 0;

        string[] menuItems = new string[] { "Играть", "Задать количество попыток", "Задать минимальное число диапазона", "Задать максимальное число диапазона", "Выход" };
        while (true)
        {
            Console.Clear();
            index = Menu.MenuLogic(menuItems, index);
            configure = MainMenu(index, args, configure);
        }
    }
    private static Configure MainMenu(int index, string[] args, Configure configure)
    {
        switch (index)
        {
            case 0:                
                Play play = new Play();
                play.StartGame(configure);
                break;
            case 1:
                configure.Tries();
                break;
            case 2:
                configure.LowerRange();                
                break;
            case 3:
                configure.UpperRange();
                break;
            case 4:
                Environment.Exit(-1);
                break;
            default:
                break;
        }
        return configure;
    }
}